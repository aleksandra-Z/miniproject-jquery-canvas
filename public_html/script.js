$(document).ready(function()
{  
    var canvas = document.getElementById("canvas");
    
    context = canvas.getContext("2d");
    
    $("#canvas").mousemove(function(e)
    {
        var x = xOffset(e, $(this));
        var y = yOffset(e, $(this));
        
        $("#pos").html("X: "+x+", Y: "+y);
    });

    var i=0;
    
    $("#canvas").click(function(e){
       var x = xOffset(e, $(this)); 
       var y = yOffset(e, $(this)); 
       
       if(++i % 2)
           context.moveTo(x, y);
       else{
           context.lineTo(x, y);
           context.stroke();
       }
   
    });
    
    $("#erase").click(function(){
       context.clearRect(0,0, 500, 500); 
    });
    
    function xOffset(e, object)
    {
        return e.pageX - object.offset().left;
    }
    function yOffset(e, object)
    {
        return e.pageY - object.offset().top;
    }    
});
